<?php
  class Router {
    static function route() {
      //foreach ($_REQUEST as $key => $value) { echo $key, " => ", $value, "<br>"; }
      $controller_name = empty($_REQUEST["controller"]) ? "index": $_REQUEST["controller"];
      $action_name = empty($_REQUEST["action"]) ? "index" : $_REQUEST["action"];

      $controller_file = "app/controllers/".$controller_name.'_controller.php';
      if (file_exists($controller_file)) {
        include $controller_file;
      } else {
        exit("Ошибка! Файл контроллера $controller_file не найден.");
      }
      $controller_class_name = ucfirst($controller_name).'Controller';
      $controller = new $controller_class_name;

      $model_file = "app/models/".$controller_name.'_model.php';
      if (file_exists($model_file)) {
        include $model_file;
      } else {
        exit("Ошибка! Файл модели $model_file не найден.");
      }
      $model_class_name = ucfirst($controller_name).'Model';
      $model = new $model_class_name;

      $controller->model = $model;

      if (method_exists($controller, $action_name)) {
        $controller->$action_name();
      } else {
        exit("Ошибка! Отсутствует метод $action_name контроллера $controller_class_name.");
      }
    }
  }
?>
