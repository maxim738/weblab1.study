<?php
require 'app/models/validators/results_verification.php'; 
class TestModel extends Model {

  function __construct() {
    $this->validator = new ResultsVerification();
    $this->set_test_validation_rule();
  }

  public function set_test_validation_rule() {
    if ($this->validator == null) {
      return false;
    }
    $this->validator->setRule("fullName", "isNotEmpty");
    $this->validator->setRule("fullName", "isFIO");
    $this->validator->setRule("group", "isNotEmpty");
    $this->validator->setRule("asw_1", "checkQues1");
    $this->validator->setRule("var1", "checkQues2Var1");
    $this->validator->setRule("var2", "checkQues2Var2");
    $this->validator->setRule("asw_3", "isSelectComboBoxQues3");
    $this->validator->setRule("asw_3", "checkQues3");

    return true;
  }

} 
?>
