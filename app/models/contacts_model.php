<?php
require 'app/models/validators/form_validation.php'; 
class ContactsModel extends Model {

  function __construct() {
    $this->validator = new FormValidation();
    $this->set_contacts_validation_rule();
  }

  public function set_contacts_validation_rule() {
    if ($this->validator == null) {
      return false;
    }
    $this->validator->setRule("fullName", "isNotEmpty");
    $this->validator->setRule("fullName", "isFIO");
    $this->validator->setRule("user_email", "isNotEmpty");
    $this->validator->setRule("user_email", "isEmail");
    $this->validator->setRule("telNumber", "isNotEmpty");
    $this->validator->setRule("user_email", "isNotEmpty");
    return true;
  }

} 
?>
