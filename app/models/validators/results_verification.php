<?php
require_once 'app\models\validators\custom_form_validation.php';

class ResultsVerification extends CustomFormValidation {


  protected function checkQues2Var1($data) {
    if ($data != 'on') {
      return true;
    } else {
      return "Ответ на вопрос 2 неверный.";
    }
  }
  protected function checkQues2Var2($data) {
    if ($data == 'on') {
      return true;
    } else {
      return "Ответ на вопрос 2 неверный.";
    }
  }
  protected function isSelectComboBoxQues3($data)
  {
    if($data == "not_selected")
      return "Ответ на вопрос 3 не выбран!";
    else return true;
  }
  protected function checkQues3($data) {
    if ($data == 'Только в твердой среде') {
      return true;
    } else {
      return "Ответ на вопрос 3 неверный.";
    }
  }

}
?>
