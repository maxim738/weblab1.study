<?php
require_once 'app\models\validators\form_validation.php';

class CustomFormValidation extends FormValidation {

  protected function checkQues1($data) {
    if ($data == "явление электромагнитной индукции заключается в возникновении электрического тока в проводящем контуре") {
      return true;
    } else {
      return "Ответ на вопрос 1 неверный.";
    }
  }

}
?>
