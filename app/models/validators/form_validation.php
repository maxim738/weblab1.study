<?php
class FormValidation {

  protected $Rules;
  protected $Errors;


  protected function isNotEmpty($data, $field) {
    if (!empty($data)) {
      return true;
    } else {
      return "Поле $field является пустым!";
    }
  }

  protected function isInteger($data, $field) {
    if (is_numeric($data)) {
      return true;
    } else {
      return "Значение поля $field ($data) не является числом.";
    }
  }

  protected function isLess($data, $field, $value = 0) {
    if (is_numeric($data) || $data >= $value) {
      return true;
    } else {
      return "Значение поля $field ($data) не является числом или число меньше заданного ($value).";
    }
  }

  protected function isGreater($data, $field, $value = 0) {
    if (is_numeric($data) || $data <= $value) {
      return true;
    } else {
      return "Значение поля $field ($data) не является числом или число больше заданного ($value).";
    }
  }

  protected function isEmail($data) {
    if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
      return true;
    } else {
      return "E-mail ($data) указан некорректно.";
    }
  }

  protected function isFIO($data) {
    if (count(preg_split('/\s+/', $data)) == 3) {
      return true;
    } else {
      return "ФИО ($data) введено некорректно.";
    }
  }

  public function setRule($field_name, $validator_name) {
    $this->Rules[$field_name][] = $validator_name;
  }

  public function validate($post_array) {
    foreach ($post_array as $post_array_key => $post_array_value) {
      if (array_key_exists($post_array_key, $this->Rules)) {
        foreach ($this->Rules[$post_array_key] as $rule_method) {
          $safeValue = trim(stripslashes(strip_tags(htmlspecialchars($post_array_value))));
          $ans = $this->$rule_method($safeValue, $post_array_key);
          if ($ans !==  true) {
            $this->Errors[] = $ans;
          }
        }
      }
    }
  }

  public function showErrors() {
    if ($this->Errors == null) {
      return false;
    }
    foreach ($this->Errors as $key => $value) {
      echo "$key - $value<br>";
    }
  }

  public function getErrors() {
    if ($this->Errors == null) {
      return false;
    } else {
      return $this->Errors;
    }
  }

}
?>
