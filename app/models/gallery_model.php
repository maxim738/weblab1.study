<?php
class GalleryModel extends Model 
{
    public const PHOTOS = [
        [ "url" => "/public/assets/img/gallery/1.jpg", "title" => "Moscow-City" ],
        [ "url" => "/public/assets/img/gallery/2.jpg", "title" => "Башня Империя" ],
        [ "url" => "/public/assets/img/gallery/3.jpg", "title" => "Красная площадь" ],
        [ "url" => "/public/assets/img/gallery/4.jpg", "title" => "Собор Василия Блаженного" ],
        [ "url" => "/public/assets/img/gallery/5.jpg", "title" => "Парк Зарядье" ],
        [ "url" => "/public/assets/img/gallery/6.jpg", "title" => "Рябина" ],
        [ "url" => "/public/assets/img/gallery/7.jpg", "title" => "Спорткары" ],
        [ "url" => "/public/assets/img/gallery/8.jpg", "title" => "Пицца" ],
        [ "url" => "/public/assets/img/gallery/9.jpg", "title" => "Колесо обозрения" ],
        [ "url" => "/public/assets/img/gallery/10.jpg", "title" => "Исаакиевский собор" ],
        [ "url" => "/public/assets/img/gallery/11.jpg", "title" => "Собор" ],
        [ "url" => "/public/assets/img/gallery/12.jpg", "title" => "Разведение мостов" ],
        [ "url" => "/public/assets/img/gallery/13.jpg", "title" => "Собор Спас на Крови" ],
        [ "url" => "/public/assets/img/gallery/14.jpg", "title" => "Дом Зингера" ],
        [ "url" => "/public/assets/img/gallery/15.jpg", "title" => "Санкт-Петербург" ],
        [ "url" => "/public/assets/img/gallery/5.jpg", "title" => "Парк Зарядье" ]
    ];
}
?>