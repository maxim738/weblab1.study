<?php
class TestController extends Controller {
  function index() {
    $this->view->render("test_view.php", "Тест");
  }
  function checkTest() {
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
      $this->model->validate($_POST);
      $errs = $this->model->validator->getErrors();
      $this->view->render("test_view.php", "Тест", $errs);
    }
    else echo "Неверный метод запроса";
  }
}
?>