<?php
class ContactsController extends Controller {
  function index() {
    $this->view->render("contacts_view.php", "Контакты");
  }
  function checkForm() {
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
      $this->model->validate($_POST);
      $errs = $this->model->validator->getErrors();
      $this->view->render("contacts_view.php", "Контакты", $errs);
    }
    else echo "Неверный метод запроса";
  }
}
?>