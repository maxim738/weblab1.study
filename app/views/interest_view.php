<section class="interests">
        <div class="avatar">
            <img class="image" src="/public/assets/img/interest.jpg" alt="Изображение" />
        </div>
        <div class="interests__menu">
            <ul>
                <?php 
                    foreach ($data::INTERESTS as $value) {
                        echo "<li><a href=#{$value["anchor"]}>{$value["title"]}</a></li>";
                    }
                ?>
            </ul>
        </div>
        <?php 
            foreach ($data::INTERESTS as $value) {
                echo "<article id=\"{$value['anchor']}\">";
                echo "
                <h2>{$value['title']}</h2>
                        <div class=\"interests__text\">
                            <p>{$value['text']}</p>
                                <ul>
                ";
                foreach ($value["list"] as $note) {
                    echo "<li>{$note}</li>";
                }
                echo "</ul></article>";
            }
        ?>
    </section>