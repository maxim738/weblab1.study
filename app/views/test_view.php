<section class="testing">
    <span class="contacts__title">Тестирование по Физике!</span>
    <form name="data" method="post" action="checkTest" id="feedback">
        <div class="form__field">
            <p>
                <span class="form__title">Ваше ФИО:</span>
                <input type="text" size="32" name="fullName">
            </p>
        </div>
        <div class="form__field">
            <p>
                <span class="form__title">Ваша группа:</span>
                <input type="text" size="32" name="group">
            </p>
        </div>

        <div class="form__field">
            <span class="form__title">1. В чем заключается явление электромагнитной индукции?</span><br><br>
            <input type="text" size="28" name="asw_1">
        </div>
        <div class="form__field">
            <span class="form__title">2. Какая из частиц не имеет массы покоя?</span>
            <p>
                <input type="checkbox" name="var1">  
                <label for="var1">Нейтрон</label>
                <input type="checkbox" name="var2">  
                <label for="var2">Фотон</label>
            </p>
        </div>
        <div class="form__field">
            <span class="form__title">3. Упругие продольные волны могут распространяться:</span>
            <p><select name="asw_3">
                <option value="not_selected">Выберите ответ</option>
                <option value="Только в твердой среде">Только в твердой среде</option>
                <option value="В любой среде">В любой среде</option>
                <option value="Только в газе">Только в газе</option>
                <option value="Только в жидкости">Только в жидкости</option>
            </select></p>
        </div>
        <p>
            <button type="reset">Очистить форму</button>
            <button type="submit">Отправить</button>
        </p>
    </form>
    <?php
         if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if($data != null && is_array($data)){
                echo "<p>Ошибки:</p>";
                foreach($data as $err){
                    echo "<p>$err</p>";
                }
            }
            else echo "<p>Тест пройден успешно!</p>";
        }
        ?>
</section>