<section class="contacts">
    <span class="contacts__title">Всё, что нужно, чтобы связаться со мной:</span>
    <div class="contacts__socials">
        <div class="social__button">
            <a href="https://vk.com/maximan_klim"> <img src="/public/assets/img/socials/vk.png" alt="" /></a>
        </div>
        <div class="social__button">
            <a href="https://instagram.com/maxx_klim"> <img src="/public/assets/img/socials/instagram.png" alt="" /></a>
        </div>
        <div class="social__button">
            <a href="#"><img src="/public/assets/img/socials/telegram-plane.png" alt="" /></a>
        </div>
    </div>
    <span class="contacts__title"> - или - </span>
    <div class="contacts_form">
        <form name="data" method="post" action="checkForm" id="feedback">
            <div class="form__field">
                <p>
                    <span class="form__title">Ваше ФИО:</span>
                    <input name="fullName" type="text" size="32" id="fio">
                </p>
                <span class="error_label" id="fio_error"></span>
            </div>
            <div class="form__field">
                <p>
                    <span class="form__title">Ваш телефон:</span>
                    <input name="telNumber" type="tel" size="32" id="tel">
                </p>
                <span class="error_label" id="phone_error"></span>
            </div>
            <div class="form__field">
                <p>
                    <span class="form__title">Ваш E-mail:</span>
                    <input name="user_email" type="email" size="32" autocomplete="on" id="mail">
                </p>
                <span class="error_label" id="email_error"></span>
            </div>

            <div class="form__field">
                <p>
                    <span class="form__title">Сообщение:</span>
                    <textarea rows="5" cols="32" name="msg" id="message"></textarea>
                </p>
            </div>
            <p>
                <button type="reset">Очистить форму</button>
                <button type="button" id="send_form">Отправить</button>
            </p>
        </form>
        <?php
         if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if($data != null && is_array($data)){
                echo "<p>Ошибка отправки:</p>";
                foreach($data as $err){
                    echo "<p>$err</p>";
                }
            }
            else echo "<p>Данные успешно отправлены!</p>";
        }
        ?>
    </div>
</section>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.js"></script>
<script src="/public/assets/js/pophint.js"></script>