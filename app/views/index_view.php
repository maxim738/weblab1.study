<section class="intro">
    <div class="intro__name">
        <span><b>Максим<br />Климашин</b></span>
    </div>
    <div class="intro__title">
        <span>20 лет, Севастополь<br>.Net | Javascript разработчик</span>
    </div>
    <div class="avatar">
        <img class="image" src="/public/assets/img/intro_2.jpg" alt="Изображение" />
    </div>
</section>
<section class="info">
    <div class="info__title">
        <span>Информация</span>
    </div>
    <div class="info_description">
        <span>
            Группа ИС/б-17-1<br>
            Лабораторная работа №5<br>"Исследование архитектуры MVC приложения и возможностей обработки данных HTML-форм на стороне сервера с использованием языка PHP"
        </span><br>
    </div>
</section>