</html>
<!DOCTYPE>
<html>
<head>
    <meta charset="utf8">
    <title>Сайт Климашина М.К. | <?=$title?></title>
    <link rel="stylesheet" href="/public/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <nav>
            <a class="nav__link <?=$title=='Главная'?'current':''?>" href="/">Главная</a>
            <a class="nav__link <?=$title=='О себе'?'current':''?>" href="/about/">О себе</a>
            <div class="nav__submenu">
                <a class="nav__link <?=$title=='Мои интересы'?'current':''?>" href="/interest/">Мои интересы</a>
                <ul class="sub__container">
                    <li><a class="nav__link" href="/interest/#hobby">Моё хобби</a></li>
                    <li><a class="nav__link" href="/interest/#films">Мои фильмы</a></li>
                    <li><a class="nav__link" href="/interest/#music">Моя музыка</a></li>
                    <li><a class="nav__link" href="/interest/#books">Мои книги</a></li>
                </ul>
            </div>
            <a class="nav__link <?=$title=='Учёба'?'current':''?>" href="/education/">Учёба</a>
            <a class="nav__link <?=$title=='Фотоальбом'?'current':''?>" href="/gallery/">Фотоальбом</a>
            <a class="nav__link <?=$title=='Контакты'?'current':''?>" href="/contacts/">Контакты</a>
        </nav>
        <div class="separator"></div>
    </header>
    <?php
        require $content_view;
    ?>
<footer>
        &copy; <a href="https://slar.ru">slar.ru</a> | <a href="https://vk.com/maximan_klim">Maxim Klimashin</a> 2019
    </footer> <script src="/public/assets/js/submenu.js"></script>
</body>
</html>
