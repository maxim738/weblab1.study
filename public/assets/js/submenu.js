﻿var el = document.getElementsByClassName('nav__submenu');


for (var i = 0; i < el.length; i++) {
    var element = el[i];
    element.addEventListener("mouseenter",
        () => switcher(element, true), false);
    element.addEventListener("mouseleave",
        () => switcher(element, false), false);
}

function switcher(element, show) {
    for (var i = 0; i < element.childNodes.length; i++)
        if (element.childNodes[i].className === "sub__container")
            element.childNodes[i].style.display = show? "block" : "none";
}

// function getMonthString(date) {
//     let months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
//     return months[date.getMonth()-1];
// }

// function timeset() {
//     let date = new Date();
//     let year = String(date.getFullYear()).slice(-2);
//     let timeS = `Дата:  ${date.getDate()} ${getMonthString(date)} ${year} | Время: ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
//      document.getElementById('time').innerText = timeS;
// }
// timeset();
// setInterval(timeset, 1000);