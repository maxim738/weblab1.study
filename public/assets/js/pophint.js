﻿$(() => {
    $('#send_form').on("click",function () {
        printWindow("Отправить данные?");
        $('.blurredBackground .yes').click(function () {
            $('#feedback').submit();
        });
    });
    addMessageBox("fio", "Данное поле должно сожержать 3 слова");
    addMessageBox("mail", "Введите свой e-mail");
    addMessageBox("tel", "Телефон должен начинаться с +7 (+3). Количество цифир в телефона от 10 до 12");
    addMessageBox("message", "Ваше сообщение");
});


function printWindow(messegeText) {
    var timeInOut = 350;
    var newElement = $('<div class=blurredBackground><figure><p>' + messegeText + '</p><span class="yes"> Да </span><span class="no"> Нет </span></figure></div>');
    $('.contacts').after(newElement);
    var $modalWindow = $('.blurredBackground');
    $modalWindow.fadeIn(timeInOut);
    // нажатие "Да"
    $('.blurredBackground .yes').click(function () {
        $modalWindow.fadeOut(timeInOut, function () {
            $(this).remove();
        });
    });
    // нажатие "Нет"
    $('.blurredBackground .no').click(function () {
        $modalWindow.fadeOut(timeInOut, function () {
            $(this).remove();
        });
    });
}

function addMessageBox(itemId, messageText) {
    var timeInOut = 150;
    var timeForOut = 1000;
    $("#" + itemId).mouseover(function () {
        var newElems = $('<div class=messageBox>' + messageText + '</div>');
        $(this)
            .queue(function (go) {
                $(this).after(newElems);
                go();
            })
            .queue(function (go) {
                $(this).next().fadeIn(timeInOut);
                go();
            });
    });
    $("#" + itemId).mouseout(function () {
        if ($(this).next().hasClass("messageBox")) {
            $(this)
                .next()
                .queue(function (go) {
                    $(this).delay(timeForOut);
                    go();
                })
                .queue(function (go) {
                    $(this).fadeOut(timeInOut, function () {
                        $(this).remove();
                    });
                    go();
                });
        }
    });
}